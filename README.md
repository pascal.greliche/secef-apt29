# SECEF Project  

## Preamble

We are two Cybersecurity students in CentraleSupélec who worked during our Master2 degree (2020-2021) on [SECEF Project](https://www.secef.net/).  
  
In the context of this project, we studied the [AT29 scenario](https://github.com/mitre-attack/attack-arsenal/tree/master/adversary_emulation/APT29/Emulation_Plan/Day%201) and we created an virtualized environment to reproduce it.  
  
The mail goal involving the reproduction of this APT29 scenario was to study IDMEF format (its problematics, limitations, ...) and evaluate its capabilities, efficiency & pertinence based on a real & recent intrusion detection scenario initiated by an Advanced Persistent Threat group.  
  
Thus, the goal of this repository is to provide the resources to reproduce the APT29 scenario within the test environment implemented on CentraleSupélec server.  


## Students
- [Quynh-Nhien PHAN](https://gitlab-student.centralesupelec.fr/quynh-nhien.phan)
- [Bastien LE GUERN](https://gitlab-student.centralesupelec.fr/bastien.le-guern)
