# APT29 - SECEF INFRASTRUCTURE ENVIRONMENT MANAGEMENT

This file lists all the aliases used to simply the environment's virtual machines management.  

## List all VMs
```
alias list='VBoxManage list vms'
```

## List all running VMs
```
alias listr='VBoxManage list runningvms'
```
## Start all the environment's VMs
```
alias start-all='VBoxManage startvm Manager --type headless \
	&& VBoxManage startvm Attacker --type headless \
	&& VBoxManage startvm WinServer --type headless \
	&& VBoxManage startvm CLIENT1 --type headless \
	&& VBoxManage startvm CLIENT2 --type headless'
```

## Stop all the environment's VMs
```
alias stop-all='VBoxManage controlvm Manager poweroff \
	&& VBoxManage controlvm Attacker poweroff \
	&& VBoxManage controlvm WinServer poweroff \
	&& VBoxManage controlvm CLIENT1 poweroff \
	&& VBoxManage controlvm CLIENT2 poweroff'
```

## Reset all the environment's VMs to their initial stat (before the APT29 scenario)
```
alias reset-scenario='VBoxManage snapshot Manager restore "Init state" \
	&& VBoxManage snapshot Attacker restore "Init state" \
	&& VBoxManage snapshot WinServer restore "Init state" \
 	&& VBoxManage snapshot CLIENT1 restore "Init state" \
	&& VBoxManage snapshot CLIENT2 restore "Init state"'
```

## Handle Manager's VM
```
alias siem='VBoxManage startvm Manager --type headless'
alias stop-siem='VBoxManage controlvm Manager poweroff'
alias restore-siem='VBoxManage snapshot Manager restorecurrent'
alias ssh-siem='ssh -p 5623 user@127.0.0.1'
alias snap-siem='VBoxManage snapshot Manager list'
```

## Handle Attacker's VM
```
alias atk='VBoxManage startvm Attacker --type headless'
alias stop-atk='VBoxManage controlvm Attacker poweroff'
alias restore-atk='VBoxManage snapshot Attacker restorecurrent'
alias ssh-atk='ssh -p 5622 user@127.0.0.1'
alias snap-atk='VBoxManage snapshot Attacker list'
```

## Handle WinServer's VM
```
alias ad='VBoxManage startvm WinServer --type headless'
alias stop-ad='VBoxManage controlvm WinServer poweroff'
alias restore-ad='VBoxManage snapshot WinServer restorecurrent'
alias ssh-ad='ssh -p 5624 Administrateur@127.0.0.1'
alias snap-ad='VBoxManage snapshot WinServer list'
```

## Handle CLIENT1's VM
```
alias c1='VBoxManage startvm CLIENT1 --type headless'
alias stop-c1='VBoxManage controlvm CLIENT1 poweroff'
alias restore-c1='VBoxManage snapshot CLIENT1 restorecurrent'
alias ssh-c1='ssh -p 5625 secef\\user1@127.0.0.1'
alias snap-c1='VBoxManage snapshot CLIENT1 list'
```

## Handle CLIENT2's VM
```
alias c2='VBoxManage startvm CLIENT2 --type headless'
alias stop-c2='VBoxManage controlvm CLIENT2 poweroff'
alias restore-c2='VBoxManage snapshot CLIENT2 restorecurrent'
alias ssh-c2='ssh -p 5626 secef\\user2@127.0.0.1'
alias sshadmin-c2='ssh -p 5626 secef\\user1@127.0.0.1'
alias snap-c2='VBoxManage snapshot CLIENT2 list'
```