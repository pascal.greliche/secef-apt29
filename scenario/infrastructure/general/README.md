# APT29 - SECEF INFRASTRUCTURE GENERAL INFORMATION

## Attacker (APT29's machine) | Ubuntu 18.04.03 LTS 

### Credentials 
```
USER ACCOUNT (sudoer)
	username : user  
	password : user

ROOT ACCOUNT  
	username : root  
	password : root  
```

### Other

In user's home directory (APT29 directory):
```
exfiltrated-data	:	directory that will contain all the data exfiltrated by the Attacker during the APT29 scenario
payloads		:	directory that contains all the payload used by the Attacker during the APT29 scenario
pupy			:	directory that contains PupyRAT files 
```  
Metasploit's folder is available at `/opt/metasploit-framework`.



## Manager (SIEM) | CentOS 8.3.2011 

### Credentials

~~USER ACCOUNT  
	username : user  
	password : user~~

```
ROOT ACCOUNT (+ MySQL DB ACCESS)    
	username : root  
	password : root

PREWIKKA ACCESS  
	username : admin  
	password : admin  
```

### SIEM tools - Scripts

In root's home directory (APT29 directory):
```
start-all.sh 	: 	launch all the IDS installed/configured & their tools associated to generate the IDMEF alerts during the APT29 scenario (Prelude tools, Suricata, Barnyard2, Snort, OSSEC Manager & Prewikka)
stop-all.sh 	: 	stop all the IDS launched by start-all.sh
```  
NIDS, HIDS & Prelude tools can be launched separately by going in 
start-scripts directory located in APT29's root directory, and exeucting 
the script wanted. Each start-script has its corresponding stop-script in 
the stop-scripts directory also located in APT29's root directory.

### SIEM tools - Directories/Files configuration  

```
SIEM
	Prelude 		:	/etc/prelude
	Prelude-Manager 	: 	/etc/prelude-manager
	Prelude-LML 		: 	/etc/prelude-lml 
	Prelude-Correlator 	: 	/etc/prelude-correlator 
	Prewikka 		:	/etc/prewikka
HIDS
	OSSEC HIDS Manager 	: 	/var/ossec/

NIDS 
	Snort			:	/etc/snort/
	Barnyard2 		:	/usr/src/barnyard2 /etc/snort/barnyard2.conf
	Suricata 		: 	/etc/suricata
```
## WinServer (Active Directory) | Windows Server 2016 Standard x1607 

### Credentials
```
ADMINISTRATOR ACCOUNT  
	username : SECEF\Administrateur  
	password : ActiveD78 (Expired => ActiveD79)
```

## CLIENT1 (or Windows Host 1) | Windows 10 Professional x1903

### Credentials 
```
ADMINISTRATOR ACCOUNT  
	username : SECEF\user1  
	password : Admin78  
```

### HIDS & Malware - Scripts 
```
rcs.3aka3.doc			:	APT29 script to begin the scenario
clamav-scan-all.ps1		:	Powershell script to execute ClamAV scan on all Windows system
clamav-scan-one.ps1		:	Powershell script to execute ClamAV scan on a single file/directory (with its subdirectories and files) given as an argument to the script 
APT29Kit.zip			:	Compressed archive which contains the files described above
```

### HIDS Tools - Directories/Files configuration  

```
OSSEC HIDS Client	:	C:\Program Files(x86)\ossec-agent
ClamAV			:	C:\Program Files\ClamAV
RSyslog Agent		:	C:\Program Files(x86)\RSyslog\Agent
```

## CLIENT2 (or Windows Host 2) | Windows 10 Professional x1903

### Credentials
```
USER ACCOUNT
	username : SECEF\user2  
	password : Login78  
	
ADMINISTRATOR ACCOUNT
	username : SECEF\user1  
	password : Admin78  
```
	
### HIDS Tools - Scripts & Directories/Files configuration

(cf CLIENT1 section)



