# APT29 - DETAILS DU SCÉNARIO D'ATTAQUE 

## ETAPE 1 - Brèche initiale 

- Handler sur la machine de l'attaquant Attacker (192.168.7.2) via PupyRAT sur le port 1234 avec comme type de transport EC4 

- Sur la machine CLIENT1 (192.168.6.11), le fichier Microsoft Word rcs.3aka3.doc (fichier Word déguisé en fait en fichier d'économiseur d'écran .scr) est en réalité un exécutable Windows PE.  

- Etablissement d'une connexion sur canal C2 en direction de la machine de l'attaquant Attaquant (192.168.7.2) sur son port 1234. Connexion chiffrée avec le protocole cryptographique RC4 

- Génération d'un Shell + PowerShell interactif à distance par le biais de la connexion C2.
  

## ETAPE 2 - Collection rapide et Exfiltration

- Recherche et collection récursive de différentes extensions de documents ou médias sur la machine CLIENT1 (.doc, .pdf, etc) & compression des fichiers et envoie du fichier ZIP résultant dans %appdata%/Draft.zip

- Terminaison de la session PowerShell

- Exfiltration du fichier Draft.zip à partir de la machine CLIENT1 (192.168.6.11) via la canal C2 vers la machine Attacker (192.168.7.2) sur son port 1234. 

## ETAPE 3 - Déploiement en douce d'un toolkit 

- Handler sur la machine de l'attaquant Attacker (192.168.7.2) via Metasploit sur le port 443 avec comme type de transport HTTPS (attente d'une connexion reverse HTTPS, listen sur port 443) 

- Upload d'une image monkey.png à partir de la machine Attacker via le canal C2 créé vers le dossier Downloads de la machine CLIENT1. C'est une image légitime qui contient du code malveillant caché par stéganographie.

- Lancement d'une session PowerShell et création de clé de registre :
  - HKCU:\Software\Classes\Folder (clé)
  - HKCU:\Software\Classes\Folder\shell (clé)
  - HKCU:\Software\Classes\Folder\shell\open (clé)
  - HKCU:\Software\Classes\Folder\shell\open\command (clé)

- Ajout d'une entrée de registre (Default) dans la clé de registre  `HKCU:\Software\Classes\Folder\shell\open\command` avec comme valeur un script qui exécute dans PowerShell le code malveillant intégré dans l'image monkey.png   

- Ajout d'une entrée de registre DelegateExecute dans la clé de registre `HKCU:\Software\Classes\Folder\shell\open\command` avec une valeur vide.

- Terminaison de la session PowerShell 

- Exécution dsur le shell/CMD de sdclt.exe (ou Microsoft Windows Backup fichier, qui est utilisé dans Windows dans le contexte de la restauration système). Il utilise un mécanisme d'auto-élévation avec Sysinternals Sigcheck qui fait appel à C:\Windows\System32\control.exe qui va faire appel à `HKCU:\Software\Classes\Folder\shell\open\command\DelegateExecute` qui va lui indiquer de suivre les instructions contenues dans la valeur de l'entrée (Default)(COM hijacking).  L'exécutable sdclt.exe va donc lancer le script malveillant avec des droits administrateurs (élévation de privilèges).

- Création grâce à l'exécutable sdclt.exe d'une nouvelle connexion C2 utilisant le protocole HTTPS (TLS) sur le port 443 vers le handler lancé dans Metasploit sur la machine => 

- Utilisation de HTTP/HTTPS sous des couches d'encodage (Base64) et de chiffrement (RC4, AES)

- Nettoyage des artefacts utilisés lors de l'attaque `HKCU:\Software\Classes\Folder\shell\open\command`

- Terminaison le shell/CMD et la connexion 

## ETAPE 4 - Découverte d'informations et contournement des mécanismes de défense (si la machine est intéressante)

- Lancement d'un reverse shell obtenu sur la machine Attacker (192.168.7.2) avec Metasploit grâce au callback sur son port 443. Ouverture d'un Meterpreter.

- Upload de SysinternalsSuite.zip dans le dossier Downloads de la machine CLIENT1. Les outils contenus dans le fichier ZIP vont permettre d'augmenter les possibilités de commandes exécutables par l'attaquant à distance. 

- Lancement d'une sessions PowerShell

- Unzip de SysinternalSuite et copie du répertoire dézippé dans C:\Program Files\

- Enumeration des processus en cours sur le système et terminaison de la session PupyRAT en tuant le process rcs.3aka3.doc qui tournait en arrière-plan

- Nttoyage/Suppression avec sdelete du fichier binaire PE  $env:USERPROFILE\Desktop\cod.3aka3.scr, $env:APPDATA\Draft.Zip & $env:USERPROFILE\Downloads\SysinternalsSuite.zip

- Renommage de readme.txt en readme.ps1 dans C:\Program Files\SysinternalsSuite\

- Invoke-Discovery dans PowerShell : 
  - Enumération du nom d'utilisateur sur la machine ($env:USERNAME)
  - Enumération du chemin d'accès jusqu'au dir TEMP sur la machine ($env:TEMP)
  - Enumération du nom d'hôte de la machine ($env:COMPUTERNAME)
  - Enumération du nom de domaine de la machine ($env:USERDOMAIN)
  - Enumération du PID de la machine ($PID)
  - Enumération de la version OS (Gwmi Win32_OperatingSystem)   
  - Enumération du type d'antivirus installé sur la machine (Get-WmiObject -Namespace "root\$SecurityCenter" -Class AntiVirusProduct)     
  - Enumération du type de firewall installé sur la machine (Get-WmiObject -Namespace "root\$SecurityCenter" -Class FireWallProduct)
  - Enumération du nom du groupe global auquel l'utilisateur appartient sur le domaine (Invoke-NetUserGetGroups)
  - Enumération du nom du groupe global auquel l'utilisateur appartient localement (Invoke-NetUserGetLocalGroups)

## ETAPE 5  - Persistance

**Persistance [1]**

- Création d'un nouveau service javamtsup (Java(TM) Virtual Machine Support Service) qui exécute un fichier binaire C:\Windows\System32\javamtsup.exe au démarrage de l'ordinateur 
- Création d'une clé de registre HKLM:\SOFTWARE\Javasoft avec comme valeur un script PowerShell qui executer un callback C2 via PowerShell (javasvc) vers la machine de l'attaquant. javamtsup.exe va donc exécuter ce payload contenu dans le registre.

**Persistance [2]**

- Création d'un lien symbolique C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp\hostui.lnk dans le fichier de démarrage de l'ordinateur (Startup folder) qui s'exécute au login de l'utilisateur. 
- Création de deux fichiers  C:\Windows\System32\hostui.exe & C:\Windows\System32\hostui.bat (sert à lancer hotui.exe).
- Au démarrage, hostui.lnk référence hostui.bat, qui est donc lancé. Ce dernier lance hostui.exe qui établit une connexion avec le Meterpreter sur la machine Attacker (192.168.7.2) sur son port 443.

## ETAPE 6 - Récupération des credentials

- Lancer accesschk.exe (outil CLI légitime utilisé dans Windows) qui :
  - lit la base de données de Chrome SQL pour en extraire les données chiffrées (%APPDATALOCAL%\Google\chrome\user data\default)
  - exécute CryptUnprotectedData pour déchiffrer les mots de passe Chrome (+ performe une vérification de l'intégrité des données)

*Commentaire PGR : La verson de l'outil accesschk.exe n'est pas cohérente avec le scénario. La commande ne donne que le "usage"*

- Get-PrivateKeys : récupère et exporte le certificat PFX avec PowerShell (installé de base sur le navigateur Chrome lors de la préparation de l'attaque)
  L'attaquant vole et exfiltre les certificats cryptographiques, les clés privées et les données d'authentification dont les mots de passes de Chrome. 

- Terminaison de la session PowerShell

- Exécution de post/windows/gather/credentials/credential_collector : récupère les informations d'identification trouvées sur l'hôte et les stocke dans la base de données Meterpreter sur la machine de l'attaquant. Injecte un DLL dans lsass.exe (Local Security Authority Subsystem qui assure l'identification des utilisateurs utilisateurs du domaine ou utilisateurs locaux)

## ETAPE 7 - Collection & Exfiltration 

- Lancement d'une session PowerShell & navigation dans cd C:\Program Files\SysinternalsSuite

- Invoke-ScreenCapture : récupération d'une capture d'écran sur la machine CLIENT1 après un certain temps défini avec PowerShell

- Get-Clipboard : récupère les données marquées sur le clipboard de la machine CLIENT1 avec PowerShell

- Keystroke-Check & Get-Keystrokes : récupération des touches claviers de l'utilisateur de manière asynchrones (GetAsyncKeyState) pendant un certain temps défini avec PowerShell

- Invoke-Exfil : exfiltration des données collectées et uploadées sur la machine (nettoyage) dans Download (images de la capture d'écran, certificats, etc...) vers la machine de l'attaquant
  Compression & chiffrement avec mot de passe lolol des données dans Download vers un fichier ZIP 
  Création d'un disque réseau persistent mais temporaire sur la machine CLIENT1 (192.168.6.11) afin d'exfiltrer les données vers le serveur Apache2 sur la machine Attacker (192.168.7.2) qui implémente WebDAV
  
  

## ETAPE 8 - Movement Latéral

- Enumération des hôtes du domaine à l'aide d'une requête Lightweight Directory Access Protocol (LDAP) (Ad-Search Computer Name *) sur le port **389** du contrôleur de domaine WinServer(192.168.6.2) 

- Détection d'un second hôte et création d'une PowerShell à distance avec le même utilisateur.
  Connexion HTTP/HTTPS établie avec WinRM sur CLIENT2 sur le port **5985**

- Handler sur la machine de l'attaquant Attacker (192.168.7.2) via Metasploit sur le port 8443 avec comme type de transport HTTPS (attente d'une connexion reverse HTTPS, listen sur port 443) 

- Invoke-SeaDukeStage : Download d'un payload (SeaDuke) python.exe à partir de WebDAV qui est connecté sur la machine CLIENT1 (192.168.6.11) et envoi de ce fichier sur la machine de CLIENT2 dans \\$ComputerName\ADMIN$\Temp\python.exe (programme packed avec UPX) via le remote PowerShell (Copy-Item via protocole SMB)

- Exécution du payload envoyé. Création d'un disque réseau persistent mais temporaire sur la machine CLIENT2 (192.168.6.12)  vers le serveur Apache2 sur la machine Attacker (192.168.7.2) qui implémente WebDAV. Permet l'exfiltration des données directement à partir de CLIENT2

- Exécution du payload python.exe avec PSExec sur CLIENT2 avec le remote PowerShell sur la machine CLIENT1. 
  Établissement d'une connexion avec le Meterpreter sur la machine Attacker (192.168.7.2) port 8443. Communication avec le serveur C2 avec HTTP/HTTPS sous des couches d'encodage (Base64) et de chiffrement (RC4, AES).

## ETAPE 9 - Collection

- Lancement d'un reverse shell obtenu sur la machine Attacker (192.168.7.2) avec Metasploit grâce au callback sur son port 8443. Ouverture d'un Meterpreter.

*Commentaire PGR : D'où est sensé venir le login/pass utilisé? Pas clair*

- Upload du fichier C:\Windows\Temp\Rar.exe & C:\Windows\Temp\sdelete64.exe sur CLIENT2 avec Meterpreter

- Lancement d'une session PowerShell sur CLIENT2

- Recherche et collection récursive de différentes extensions de documents ou médias sur la machine CLIENT1 (.doc, .pdf, etc) & compression des fichiers et envoie du fichier ZIP résultant dans %appdata%/working.zip

*Commentaire PGR : confusion entre client2 et user2*

- Chiffrement du fichier %appdata%/working.zip avec C:\Windows\Temp\Rar.exe avec le mot de passe hpfGzq5yKw

- Terminaison de la session PowerShell

- Exfiltration des données C:\Users\user1\Desktop\working.zip de CLIENT2 jusqu'au Meterpreter 

- Lancement d'un shell/CMD

- Nettoyage des traces de l'attaque : suppression C:\Users\user1\AppData\Roaming\working.zip, C:\Windows\Temp\Rar.exe, C:\Users\user1\Desktop\working.zip

- Terminaison du shell/CMD, Meterpreter et msfconsole

**Il est possible ici aussi d'envoyer les outils pour prendre des captures d'écran et de clavier, déployer de nouveau malware, envoyer des malwares sur d'autres machines de l'AD, etc...**

## Etape 10 - Test de la persistance 

- Tester la persistance [1] : Relancer CLIENT1 pour regarder si un callback avec des permissions SYSTEM est obtenu dans Meterpreter sur le handle port 443 sur la machine de l'attaquant (service javamtsup)

- Tester la persistance [2] : Connexion avec l'utilisateur hôte et regarder si hostui.exe est exécuté indirectement par hostui.lnk dans le répertorie StartUp de la machine, créant un callback vers Meterpreter sur le port 443
  
Le payload persistant dans le répertoire StartUp duplique et utilise le token de explorer.exe pour démarrer le malware sur la machine.

*Commentaire PGR : La payload initiale effectue une forme de persistance : se lance en tant qu'économiseur d'écran. Mais reste bloqué ensuite sur un écran noir :(*
